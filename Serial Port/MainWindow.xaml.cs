﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO.Ports;

namespace Serial_Port
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow: Window
    {
        #region Data Memebers

        SerialPort serialPort;

        #endregion

        public MainWindow()
        {
            InitializeComponent();

            LoadPortNames();
            LoadPortBaudRates();
            LoadPortParities();
            LoadPortDataBits();
            LoadPortStopBits();
            LoadPortHandshakes();
        }

        #region Initialization and Instantiation Methods

        public void LoadPortNames()
        {
            var ports = SerialPort.GetPortNames();

            foreach( var port in ports )
            {
                ddlPort.Items.Add( port );
            }
        }

        public void LoadPortBaudRates()
        {
            // Standard baud rates for serial communications:
            // 110, 300, 600, 1200, 2400, 4800, 
            // 9600, 14400, 19200, 38400, 57600, 115200, 128000 and 256000

            ddlBaudRate.Items.Add( 110 );
            ddlBaudRate.Items.Add( 300 );
            ddlBaudRate.Items.Add( 600 );
            ddlBaudRate.Items.Add( 1200 );
            ddlBaudRate.Items.Add( 2400 );
            ddlBaudRate.Items.Add( 4800 );
            ddlBaudRate.Items.Add( 9600 );
            ddlBaudRate.Items.Add( 14400 );
            ddlBaudRate.Items.Add( 19200 );
            ddlBaudRate.Items.Add( 38400 );
            ddlBaudRate.Items.Add( 57600 );
            ddlBaudRate.Items.Add( 115200 );
            ddlBaudRate.Items.Add( 128000 );
            ddlBaudRate.Items.Add( 256000 );

            ddlBaudRate.SelectedIndex = 6;
        }

        public void LoadPortParities()
        {
            foreach( var parity in Enum.GetNames( typeof( Parity ) ) ) 
            {
                ddlParity.Items.Add( parity.ToString() );
            }

            ddlParity.SelectedIndex = 0;
        }

        public void LoadPortDataBits()
        {
            // Standard baud rates for serial communications:
            // 5 - Baudot code
            // 6 - Rarely used
            // 7 - True ASCII
            // 8 - Universally used in newer applications
            // 9 - Rarely used

           
            ddlDataBits.Items.Add( 5 );
            ddlDataBits.Items.Add( 6 );
            ddlDataBits.Items.Add( 7 );
            ddlDataBits.Items.Add( 8 );
            ddlDataBits.Items.Add( 9 );

            ddlDataBits.SelectedIndex = 3;
        }

        public void LoadPortStopBits()
        {
            foreach( var stopbit in Enum.GetNames( typeof( StopBits ) ) )
            {
                ddlStopBits.Items.Add( stopbit.ToString() );
            }

            ddlStopBits.SelectedIndex = 1;
        }

        public void LoadPortHandshakes()
        {
            foreach( var handshake in Enum.GetNames( typeof( Handshake ) ) )
            {
                ddlHandshakes.Items.Add( handshake.ToString() );
            }

            ddlHandshakes.SelectedIndex = 0;
        }

        #endregion

        #region Control Event Handlers

        private void Window_Closing( object sender , System.ComponentModel.CancelEventArgs e )
        {
            CloseSerialPort();
        }

        private void ddlPort_SelectionChanged( object sender , SelectionChangedEventArgs e )
        {
            if( IsSerialPortConfigurationValid() )
            {
                btnOpenPort.IsEnabled = true;
            }
        }

        private void ddlBaudRate_SelectionChanged( object sender , SelectionChangedEventArgs e )
        {
            if( IsSerialPortConfigurationValid() )
            {
                btnOpenPort.IsEnabled = true;
            }
        }

        private void ddlParity_SelectionChanged( object sender , SelectionChangedEventArgs e )
        {
            if( IsSerialPortConfigurationValid() )
            {
                btnOpenPort.IsEnabled = true;
            }
        }

        private void ddlDataBits_SelectionChanged( object sender , SelectionChangedEventArgs e )
        {
            if( IsSerialPortConfigurationValid() )
            {
                btnOpenPort.IsEnabled = true;
            }
        }

        private void ddlStopBits_SelectionChanged( object sender , SelectionChangedEventArgs e )
        {
            if( IsSerialPortConfigurationValid() )
            {
                btnOpenPort.IsEnabled = true;
            }
        }

        private void btnOpenPort_Click( object sender , RoutedEventArgs e )
        {
            if( IsSerialPortConfigurationValid() )
            {
                var portName = ddlPort.SelectedItem.ToString();
                var baudRate = Convert.ToInt32( ddlBaudRate.SelectedItem );
                var parity = (Parity)Enum.Parse( typeof(Parity) , ddlParity.Text );
                var dataBits = Convert.ToInt32( ddlDataBits.SelectedItem );
                var stopBits = (StopBits)Enum.Parse( typeof(StopBits) , ddlStopBits.Text );
                var handshake = (Handshake)Enum.Parse( typeof(Handshake) , ddlHandshakes.Text );

                OpenSerialPort( portName , baudRate , parity , dataBits , stopBits , handshake );
                EnableSendMessage();
                EnableRecieveMessage( false );

                btnOpenPort.IsEnabled = false;
                btnClosePort.IsEnabled = true;
            }
        }

        private void btnClosePort_Click( object sender , RoutedEventArgs e )
        {
            if( serialPort != null && serialPort.IsOpen )
            {
                CloseSerialPort();
                EnableSendMessage( false );
                EnableRecieveMessage( false );

                btnOpenPort.IsEnabled = true;
                btnClosePort.IsEnabled = false;
            }
        }

        private void btnSend_Click( object sender , RoutedEventArgs e )
        {
            SendMessage();
        }

        //private void btnReceive_Click( object sender , RoutedEventArgs e )
        //{
        //    RecieveMessage();
        //}

        #endregion

        private bool IsSerialPortConfigurationValid()
        {
            return (ddlPort.SelectedIndex != -1 && 
                ddlBaudRate.SelectedIndex != -1 && ddlParity.SelectedIndex != -1 &&
                ddlDataBits.SelectedIndex != -1 && ddlStopBits.SelectedIndex != -1 &&
                ddlHandshakes.SelectedIndex != -1 );
        }

        private bool OpenSerialPort( String portName , int baudRate , Parity parity , int dataBits , StopBits stopBits , Handshake handshake )
        {
            bool isSerialPortOpened = false;

            try
            {
                serialPort = new SerialPort( portName ,
                    baudRate , parity ,
                    dataBits , stopBits );
                serialPort.Handshake = handshake;
                serialPort.Open();
                serialPort.DataReceived += RecieveMessage;

                var time = DateTime.Now.ToShortTimeString();
                tbReceive.AppendText( $"[{time}] {serialPort.PortName} connected.\n" );

                isSerialPortOpened = true;
            }
            catch( UnauthorizedAccessException ex )
            {
                MessageBoxResult messageBox = MessageBox.Show( $"Unauthorize Access: {ex.ToString()}" );
            }

            return isSerialPortOpened;
        }

        private void CloseSerialPort()
        {
            if( serialPort != null )
            {
                serialPort.Close();

                var time = DateTime.Now.ToShortTimeString();
                tbReceive.AppendText( $"[{time}] {serialPort.PortName} disconnected.\n" );
            }
        }

        private void EnableSendMessage( bool isEnabled = true )
        {
            tbSend.IsEnabled = isEnabled;
            btnSend.IsEnabled = isEnabled;
        }

        private void EnableRecieveMessage( bool isEnabled = true )
        {
            tbReceive.IsEnabled = isEnabled;
            //btnReceive.IsEnabled = isEnabled;
        }

        private void SendMessage()
        {
            if( serialPort != null )
            {
                if( !String.IsNullOrWhiteSpace( tbSend.Text ) )
                {
                    var time = DateTime.Now.ToShortTimeString();
                    tbReceive.AppendText( $"[{time}] Sent: {tbSend.Text}\n" );

                    serialPort.Write( tbSend.Text );
                    tbSend.Text = "";
                }
                else
                {
                    tbReceive.AppendText( $"Cannot send an empty message.\n" );
                }
            }
        }

        private void RecieveMessage( object sender , SerialDataReceivedEventArgs args )
        {
            try
            {
                if( serialPort != null && serialPort.IsOpen )
                {
                    var time = DateTime.Now.ToShortTimeString();
                    var message = serialPort.ReadExisting();

                    this.Dispatcher.Invoke( (Action)(() =>
                    {
                         tbReceive.AppendText( $"[{time}] Received: {message}\n" );
                    } ) );
                }
            }
            catch( TimeoutException ex )
            {
                tbReceive.Text = "Timeout Exception: " + ex.ToString();
            }
        }
    }
}
